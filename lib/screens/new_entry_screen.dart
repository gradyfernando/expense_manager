import 'package:expense_management/models/entry.dart';
import 'package:expense_management/models/kategori.dart';
import 'package:expense_management/providers/entry_provider.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:flutter_multi_formatter/flutter_multi_formatter.dart';
import '../widgets/item_kategori.dart';
import '../providers/kategori_provider.dart';

class NewEntryScreen extends StatefulWidget {
  static const routeName = '/new-entry';

  @override
  _NewEntryScreenState createState() => _NewEntryScreenState();
}

class _NewEntryScreenState extends State<NewEntryScreen> {
  final _form = GlobalKey<FormState>();
  Kategori _selectedKategori;
  final _amountController = new TextEditingController();
  final _deskController = new TextEditingController();
  DateTime _selectedDate;

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: _selectedDate,
      firstDate: DateTime(2020),
      lastDate: DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  void _showKategoriSelection(BuildContext context) {
    showModalBottomSheet(
      context: context,
      builder: (_) {
        return Consumer<KategoriProvider>(
          builder: (ctx, kategoriData, _) => Padding(
            padding: EdgeInsets.all(1.0),
            child: ListView.builder(
              itemCount: kategoriData.items.length,
              itemBuilder: (_, i) => Column(
                children: [
                  ItemKategori(kategoriData.items[i], _selectKategori),
                  Divider(),
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  void _selectKategori(Kategori kategori) {
    setState(() {
      _selectedKategori = kategori;
    });
  }

  void _inputEntry(BuildContext ctx) {
    final isValid = _form.currentState.validate();

    print('test 3');
    if (!isValid) {
      return;
    }
    print('test 4');

    _form.currentState.save();
    print('test 5');

    final _entry = new Entry(
      id: DateTime.now().toString(),
      kategori: _selectedKategori,
      amount: double.parse(_amountController.text.replaceAll(",", "")),
      deskripsi: _deskController.text,
      tipe: 1,
      date: _selectedDate,
    );

    Provider.of<EntryProvider>(ctx, listen: false).addEntry(_entry);
    Navigator.of(ctx).pop();
  }

  @override
  void initState() {
    super.initState();
    _selectedDate = DateTime.now();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('New Entry'),
        actions: [
          IconButton(
            icon: Icon(Icons.done),
            tooltip: 'Simpan',
            onPressed: () {
              _inputEntry(context);
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _form,
          child: Padding(
            padding:
                const EdgeInsets.symmetric(vertical: 12.0, horizontal: 16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Tanggal'),
                Row(
                  children: [
                    Expanded(
                      child: OutlineButton.icon(
                        icon: Icon(
                          Icons.date_range,
                          color: Theme.of(context).accentColor,
                        ),
                        label: Expanded(
                          child: Text(
                            _selectedDate == null
                                ? '${DateFormat('dd MMMM yyyy').format(DateTime.now())}'
                                : '${DateFormat('dd MMMM yyyy').format(_selectedDate)}',
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Theme.of(context).accentColor),
                          ),
                        ),
                        onPressed: _presentDatePicker,
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text('Kategori'),
                SizedBox(
                  height: 4.0,
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.symmetric(
                      horizontal: BorderSide(
                        color: Theme.of(context).accentColor,
                        width: 1,
                      ),
                    ),
                  ),
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundColor: _selectedKategori == null
                          ? Colors.red
                          : _selectedKategori.warna,
                      child: _selectedKategori == null
                          ? null
                          : Icon(
                              _selectedKategori.iconData,
                              color: Colors.white,
                            ),
                    ),
                    title: Text(_selectedKategori == null
                        ? '-- Pilih Kategori --'
                        : _selectedKategori.nama),
                    onTap: () {
                      _showKategoriSelection(context);
                    },
                  ),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Text('Jumlah'),
                SizedBox(
                  height: 4.0,
                ),
                TextFormField(
                  validator: (value) {
                    if (value == null) {
                      return 'Masukan angka';
                    }

                    print(value);

                    try {
                      if (int.parse(value.replaceAll(",", "")) == 0) {
                        return 'Angka tidak boleh 0';
                      }
                    } catch (error) {
                      return 'Angka tidak valid';
                    }

                    return null;
                  },
                  controller: _amountController,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 25,
                  ),
                  keyboardType: TextInputType.number,
                  inputFormatters: [
                    MoneyInputFormatter(
                        useSymbolPadding: true,
                        mantissaLength: 0,
                        thousandSeparator: ThousandSeparator.None),
                  ],
                ),
                SizedBox(
                  height: 8.0,
                ),
                TextFormField(
                  validator: null,
                  controller: _deskController,
                  decoration: InputDecoration(
                    labelText: 'Deskripsi',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                  ),
                  keyboardType: TextInputType.text,
                ),
                SizedBox(
                  height: 12,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
