import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../widgets/dashboard/side_bar_button.dart';
import '../widgets/dashboard/item_expense.dart';
import '../providers/entry_provider.dart';
import 'new_entry_screen.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double statusBarHeight = MediaQuery.of(context).padding.top;

    void _openDrawer(BuildContext context) {
      Scaffold.of(context).openDrawer();
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text('Dashboard'),
        leading: Builder(
          builder: (BuildContext ctx) {
            return IconButton(
              icon: Icon(
                Icons.grid_view,
                color: Theme.of(context).primaryTextTheme.headline6.color,
              ),
              onPressed: () {
                _openDrawer(ctx);
              },
            );
          },
        ),
      ),
      drawer: Drawer(),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              width: double.infinity,
              child: AspectRatio(
                aspectRatio: 2 / 1.5,
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(32.0),
                      bottomRight: Radius.circular(32.0),
                    ),
                  ),
                  color: Theme.of(context).primaryColor,
                  margin: EdgeInsets.all(0.0),
                  elevation: 8.0,
                  child: Padding(
                    padding: EdgeInsets.only(top: statusBarHeight),
                    child: Column(
                      children: [
                        // CustomAppBar(_openDrawer),
                        Expanded(
                          child: Container(
                            width: double.infinity,
                            child: Stack(
                              children: [
                                Align(
                                  child: CircleAvatar(),
                                ),
                                Align(
                                  child: SideBarButton(),
                                  alignment: Alignment.topRight,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      'Expense',
                      style: TextStyle(fontSize: 24),
                    ),
                  ),
                  FlatButton.icon(
                    label: Text('Bulan Ini'),
                    textColor: Theme.of(context).accentColor,
                    icon: Icon(
                      Icons.filter_list_rounded,
                      color: Theme.of(context).accentColor,
                    ),
                    onPressed: () {},
                  ),
                ],
              ),
            ),
            Container(
              height: 200,
              child: Consumer<EntryProvider>(
                builder: (ctx, entryData, _) => Padding(
                  padding: EdgeInsets.all(1.0),
                  child: ListView.builder(
                    itemCount: entryData.items.length,
                    itemBuilder: (_, i) => Column(
                      children: [
                        ItemExpense(entryData.items[i]),
                        Divider(),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        child: Icon(Icons.add),
        onPressed: () {
          Navigator.of(context).pushNamed(NewEntryScreen.routeName);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      resizeToAvoidBottomPadding: false,
    );
  }
}
