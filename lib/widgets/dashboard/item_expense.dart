import 'package:expense_management/models/entry.dart';
import 'package:flutter/material.dart';

class ItemExpense extends StatelessWidget {
  final Entry entry;

  ItemExpense(this.entry);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text(entry.deskripsi),
      subtitle: Text(entry.kategori.nama),
      leading: CircleAvatar(
        backgroundColor: entry.kategori.warna,
        child: Icon(
          entry.kategori.iconData,
          color: Colors.white,
        ),
      ),
      trailing: Text(
        entry.amount.toString(),
        style: TextStyle(fontSize: 20),
      ),
      onTap: () {},
    );
  }
}
