import 'package:flutter/material.dart';

class CustomAppBar extends StatelessWidget {
  final Function(BuildContext context) openDrawer;

  CustomAppBar(this.openDrawer);

  @override
  Widget build(BuildContext context) {
    return Container(
      // color: Colors.red,
      child: Row(
        children: [
          IconButton(
            icon: Icon(
              Icons.grid_view,
              color: Theme.of(context).primaryTextTheme.headline6.color,
            ),
            onPressed: () {
              openDrawer(context);
            },
          ),
          SizedBox(
            width: 12.0,
          ),
          Text(
            'Dashboard',
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: Theme.of(context).textTheme.headline6.fontSize,
              color: Theme.of(context).primaryTextTheme.headline6.color,
            ),
          )
        ],
      ),
    );
  }
}
