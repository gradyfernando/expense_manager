import 'package:flutter/material.dart';

class SideBarButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        IconButton(
          icon: Icon(
            Icons.bar_chart_rounded,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        IconButton(
          icon: Icon(
            Icons.date_range_rounded,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        IconButton(
          tooltip: 'Log',
          icon: Icon(
            Icons.receipt_long_rounded,
            color: Colors.white,
          ),
          onPressed: () {},
        ),
        
      ],
    );
  }
}
