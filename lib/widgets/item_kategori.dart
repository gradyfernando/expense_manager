import 'package:flutter/material.dart';
import '../models/kategori.dart';

class ItemKategori extends StatelessWidget {
  final Kategori kategori;
  final Function(Kategori kategori) selectKategori;

  ItemKategori(this.kategori, this.selectKategori);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      
      title: Text(kategori.nama),
      leading: CircleAvatar(
        backgroundColor: kategori.warna,
        child: Icon(
          kategori.iconData,
          color: Colors.white,
        ),
      ),
      onTap: () {
        selectKategori(kategori);
        Navigator.of(context).pop();
      },
    );
  }
}
