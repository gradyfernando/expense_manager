import '../models/kategori.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class KategoriProvider with ChangeNotifier {
  final List<Kategori> _items = [
    Kategori('1', 'Makanan', Colors.blue, Icons.restaurant),
    Kategori('2', 'Kesehatan', Colors.red, Icons.healing),
    Kategori('3', 'Pajak', Colors.green, Icons.attach_money),
    Kategori('4', 'Pakaian', Colors.blueGrey, Icons.accessibility_new_rounded),
    Kategori('5', 'Lain-lain', Colors.pink, Icons.bookmark_rounded),
    Kategori('6', 'Transportasi', Colors.deepPurple, Icons.local_taxi),
  ];

  List<Kategori> get items {
    return [..._items];
  }
}
