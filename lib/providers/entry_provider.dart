import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../models/entry.dart';

class EntryProvider with ChangeNotifier {
  final List<Entry> _items = [];

  List<Entry> get items {
    return [..._items];
  }

  void addEntry(Entry entry) {
    _items.add(entry);
    notifyListeners();
  }
}
