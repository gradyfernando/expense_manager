import 'package:flutter/foundation.dart';
import 'kategori.dart';

class Entry {
  final String id;
  final Kategori kategori;
  final double amount;
  final String deskripsi;
  final int tipe;
  final DateTime date;

  Entry({
    @required this.id,
    @required this.kategori,
    @required this.amount,
    @required this.deskripsi,
    @required this.tipe,
    @required this.date,
  });
}
