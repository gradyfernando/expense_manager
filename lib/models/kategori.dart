import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Kategori {
  final String id;
  final String nama;
  final MaterialColor warna;
  final IconData iconData;

  Kategori(
    this.id,
    this.nama,
    this.warna,
    this.iconData,
  );
}
